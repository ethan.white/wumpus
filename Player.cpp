#include "Player.h"
#include "Map.h"
#include "MapCell.h"

Player::Player(Map *map, int startx, int starty){
    currentX = startx;
    currentY = starty;
    this->map = map;
    crashed = false;
    
}

Player::~Player()
{
}

bool Player::getCrashed(){
    return crashed;
}
void Player::addAmmo(){
    ammo++;
}

void Player::printSurroundings(){
    
}

// move robot in specified direction (e/w/s/n), returning true
//   if was able to move in that direction
// Warning: this method can get long - be sure to write private messages
//   to break it up
bool Player::move(char direction){
    bool moved = false;
    
    switch(tolower(direction)){
        case 'e':
            if (map->getWidth() <= currentX+1 || map->getCell(currentX+1, currentY)->hasTrap()){
                
                crashed = true;
            } else {
                currentX++;
                moved = true;
            }
            break;
        case 'w':
            if (0 > currentX-1 || map->getCell(currentX-1, currentY)->hasTrap()){
                
                crashed = true;
            } else {
                currentX--;
                moved = true;
            }
            break;
        case 's':
            if (map->getHeight() <= currentY+1 || map->getCell(currentX, currentY+1)->hasTrap()){
                
                crashed = true;
            } else {
                currentY++;
                moved = true;
            }
            break;
        case 'n':
            if (0 > currentY-1 ||map->getCell(currentX, currentY-1)->hasTrap()){
               
                crashed = true;
            } else {
                currentY--;
                moved = true;
            }
            break;
    }
    
    return moved;
}

MapCell* Player::getCurrentCell(){
    return map->getCell(currentX, currentY);
}
bool Player::attack(string weapon, string direction){
    return true;
}