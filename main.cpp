#include <iostream>
#include <string>
#include "Map.h"
#include "Player.h"

using namespace std;

int main(int argc, char **argv)
{
	string input = "";
    Map* map = new Map();
    Player* sohoni = new Player(map, 1, 1);
    while(input != "q"){
        sohoni->printSurroundings();
        cout << endl;
        cout << "Which Action Would You Like To Perform" << endl;
        cout << "Press M) ove, A) ttack, P) ickup, or Q) uit" << endl;
        getline(cin, input);
        char test = tolower(input.at(0));
        input = tolower(input.at(0));
        switch (test){
            case 'p':
                if(sohoni->getCurrentCell()->hasAmmo()){
                    sohoni->addAmmo();
                }
                break;
            case 'm':
                cout << "Which Direction Would You Like To Move" << endl;
                cout << "Press N) orth, E) ast, S) outh, W) est, G) o back, or Q) uit" << endl;
                getline(cin, input);
                input = tolower(input.at(0));
                if(input != "q"){
                    sohoni->move(input.at(0));
                    if(sohoni->getCrashed()){
                        input = "q";
                        cout << "Esubmit crashed do to sohoni walking into ";
                        if(sohoni->getCurrentCell()->hasTrap()){
                            cout << "an infinite loop" << endl;
                        }
                        if(sohoni->getCurrentCell()->hasStudent()){
                            cout << "a student with bad code" << endl;
                        }
                    }
                }
                break;
            case 'a':
                cout << "Which Weapon Would You Like To Use" << endl;
                cout << "D) ebugger, or B) ad Grade or Q) uit" << endl;
                string secondaryInput = "";
                getline(cin, secondaryInput);
                secondaryInput = tolower(secondaryInput.at(0));
                char testTwo = tolower(secondaryInput.at(0));
                string weapon = secondaryInput;
                switch (testTwo){
                    case 'd':
                    case 'b':
                    cout << "Which Direction Would You Like To Attack" << endl;
                    cout << "Press N) orth, E) ast, S) outh, W) est, G) o back, or Q) uit" << endl;
                    getline(cin, secondaryInput);
                    input = tolower(secondaryInput.at(0));
                    if(secondaryInput != "q"){
                        sohoni->attack(weapon, secondaryInput);
                    }
                    break;
                    case 'q':
                        input = secondaryInput; 
                        break;
                    default:
                        break;
                }
                
                break;
            
        }
    }
    delete sohoni;
	return 0;
}
