/*
 * map.cpp
 *
 *      Author: Alex Ottelien
 */

#include "Map.h"
#include <iostream>
#include <string>
#include "MapCell.h"


using namespace std;

static constexpr int  WIDTH = 20;
static constexpr int  HEIGHT = 10;
MapCell *cells[WIDTH][HEIGHT];


Map::Map(){
      for(int i = 0; i < WIDTH; i++){
          for(int x = 0; x < HEIGHT; x++){
              cells[i][x] = nullptr;
          }
      }
}
  // read the map from cin
void Map::load(){
    string line;
    getline(cin, line);
    //lines are 22 wide and 12 high, write code to get around this
    for(int y = 0; y < HEIGHT; y++){
        getline(cin, line);
        for(int x = 1; x < WIDTH+1; x++){
            MapCell *cell = new MapCell(x-1, y, line.at(x));
            cells[x-1][y] = cell;
        }
    }
    getline(cin, line);
}
  // write the full map to cout
void Map::write(){
    cout << "+--------------------+" << endl;
    for(int y = 0; y < HEIGHT; y++){
        cout << "|";
        for(int x = 0; x < WIDTH; x++){
//            cout << cells[x][y]->display();
        }
        cout << "|" << endl;
    }
    cout << "+--------------------+" << endl;
}
//this method returns a cell in cells
MapCell* Map::getCell(int x, int y){
    return cells[x][y];
}