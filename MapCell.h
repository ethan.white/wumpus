/*
 * cell.h
 *
 *      Author: Alex Ottelien
 */

#ifndef MAPCELL_H_
#define MAPCELL_H_
#include <iostream>
// Tracks the contents of a particular cell on the map.
// Is there gold at this location, is it occupied by a robot,
// is there a pit there? Each cell needs to track
// where it is on the grid (x, y) and the type of
// object at that location. The provided methods are suggestions;
// feel free to add/remove as needed.
class MapCell {
  int xLocation, yLocation;
  char token;
  char originalToken;
  bool hasRobot;
  bool ammo;
  bool trapped;
public:
  MapCell(int x, int y, char type);
    bool hasStudent();
    
    bool hasTrap();
    
    bool isOccupied();
    
    char getToken();
    
    virtual void enter();
    
    void vacate();
    bool hasAmmo();
    void disarm();
  
};
class trapLoop : public MapCell {
    int xLocation, yLocation;
    char token;
    char originalToken;
    bool hasRobot;
    
    public:
    trapLoop(int x, int y, char type);
    
    bool hasStudent();
    
    bool hasTrap();
    
    bool isOccupied();
    
    char getToken();
    
    virtual void enter();
    
    void vacate();
 
};

#endif /* CELL_H_ */

