/*
 * map.h
 *
 *      Author: Alex Ottelien
 */

#ifndef MAP_H_
#define MAP_H_

#include <iostream>
#include <string>
// the following line (declaration) ensures do not to include cell.h
class MapCell;

// Track the area the robot in which the robot moves.
// Note you will need to add declarations for the height and width.
// Add other methods and data as needed.
class Map {

    static constexpr int  WIDTH = 20;
    static constexpr int  HEIGHT = 10;
private:
    MapCell *cells[WIDTH][HEIGHT];
public:
    
  // initialize empty map
  Map();
  // read the map from cin
  void load();
  // write the full map to cout
  void write();
  
  MapCell* getCell(int x, int y);
  int getWidth();
  int getHeight();
};

#endif /* MAP_H_ */

