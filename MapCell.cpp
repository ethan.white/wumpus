/*
 * cell.cpp
 *
 *      Author: Alex Ottelien
 */

#include "MapCell.h"
#include <iostream>

using namespace std;

  int xLocation, yLocation;
  char token;
  char originalToken;
  bool hasRobot;

MapCell::MapCell(int x, int y, char type){
      this->xLocation = x;
      this->yLocation = y;
      this->token = type;
      if(token != '#' && token != '*'){
          originalToken = type;
      }else{
          originalToken = ' ';
      }
      trapped = false;
}

bool MapCell::hasStudent() {
    return token == 'S';
}
  // true if pit at this location
bool MapCell::hasTrap() {
    if(token == '#'){
        return true;
    }else{
        return false;
    }
}
  // is this location occupied by something that the robot
  //   cannot move over?
bool MapCell::isOccupied(){
    return token == 'P';
}
  // return the character to display for this location
char MapCell::getToken(){
    return token;
}

void MapCell::enter() {
    originalToken = token;
    token = 'P';
}
bool MapCell::hasAmmo(){
    return ammo;
}
void MapCell::disarm(){
    trapped = false;
}

 void trapLoop::enter() {
    
}

void MapCell::vacate() {
    token = originalToken;
}