#ifndef VIRTUALWEAPON_H
#define VIRTUALWEAPON_H
class MapCell;

class virtualWeapon
{
public:
    virtual bool attack(MapCell* cell);
};


class debugger : public virtualWeapon {
private:
    int ammo = 1;
public:
    bool attack(MapCell* cell);
    void increaseAmmo();
};
class badGrade : public virtualWeapon {
public:
    bool attack(MapCell* cell);
};

#endif // VIRTUALWEAPON_H
