#ifndef PLAYER_H
#define PLAYER_H
#include <string>
#include "Map.h"
#include "MapCell.h"

using namespace std;

class Player {
    int ammo;
public:
    Player(Map* map, int startx, int starty);
    ~Player();
    bool move(char direction);
    int currentX;
    int currentY;
    Map *map;
    bool crashed;
    void printSurroundings();
    bool getCrashed();
    MapCell* getCurrentCell();
    bool attack(string weapon, string direction);
    void addAmmo();
};

#endif // PLAYER_H
